import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: string = '';
  password: string = '';
  wrongCredentials: boolean = false;

  constructor(private auth: AuthService, private router: Router, private route: ActivatedRoute) {
    if (this.auth.getAuthStatus()) this.router.navigate(['/form']);
  }

  ngOnInit() {
    this.route.queryParamMap
      .subscribe(params => {
        this.wrongCredentials = JSON.parse( params.get('wrongCredentials') );
      });
  }

  login() {
    this.auth.login(this.username, this.password);
  }

}
