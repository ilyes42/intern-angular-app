import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

const HttpOptions = {
  headers: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
  }
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  loggedIn: boolean = JSON.parse(localStorage.getItem('loggedIn')) || false;

  constructor(private http: HttpClient, private router: Router) { }

  getAuthStatus() {
    return this.loggedIn;
  }

  login(username: string, password: string) {
    this.http
      .post('http://localhost:1337/login', {
        username: username,
        password: password
      }, HttpOptions)
      .subscribe(data => {
        if (data['authenticated']) {
          this.loggedIn = true;
          localStorage.setItem('loggedIn', 'true');
          localStorage.setItem('username', data['username']);
          this.router.navigate(['/form']);
        } else {
          this.router.navigate(['/login'], { queryParams: { wrongCredentials: true } });
        }
      });
  }

  logout() {
    this.loggedIn = false;
    localStorage.removeItem('loggedIn');
    localStorage.removeItem('username');
    this.router.navigate(['/login']);
  }

}
