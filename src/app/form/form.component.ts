import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

import * as html2canvas from 'html2canvas';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  constructor(private fb: FormBuilder, private http: HttpClient, private router: Router, private auth: AuthService) { }

  ngOnInit() { }

  submitted: boolean = false;
  loading: boolean = false;
  error: boolean = false;
  screenshotsToUpload = [];

  screenshotsValidator = (control: AbstractControl): { [key: string]: boolean } | null => {
    if (control.value.length < 1) return { 'noScreenshotChosen': true }
    return null;
  }

  issueForm: FormGroup = this.fb.group({
    'summary': ['', Validators.required],
    'description': ['', Validators.required],
    'screenshots': [this.screenshotsToUpload, Validators.compose([this.screenshotsValidator])],
    'date': [''],
    'priority': ['0', Validators.required],
    'issueType': ['0', Validators.required]
  });

  summary = this.issueForm.controls['summary'];
  description = this.issueForm.controls['description'];
  screenshots = this.issueForm.controls['screenshots'];
  date = this.issueForm.controls['date'];
  priority = this.issueForm.controls['priority'];
  issueType = this.issueForm.controls['issueType'];

  log: string = "";

  onScreenshotChange = (event: Event) => {
    console.log(event);

    if ((<HTMLInputElement>event.target).files && (<HTMLInputElement>event.target).files.length) {
      const files = Array.from((<HTMLInputElement>event.target).files);
      for (const file of files) {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
          this.screenshotsToUpload.push(reader.result);
          console.log(this.screenshotsToUpload);
          this.screenshots.setValue(this.screenshotsToUpload);
          this.screenshots.updateValueAndValidity();
        }
      }
    }
  }

  onSubmit = () => {
    this.loading = true;
    setTimeout(() => {
      this.date.setValue(this.getDate());
      let data = {
        summary: this.summary.value,
        description: this.description.value,
        screenshots: this.screenshotsToUpload.join('%'),
        logfile: 'C:\\chrome_log_file.log',
        date: this.date.value,
        priority: this.priority.value,
        issueType: this.issueType.value,
        nameOfReporter: localStorage.getItem('username')
      }
      this.http
        .post('http://localhost:1337/api/issues', data, {
          headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
          }
        })
        .subscribe(data => {
          this.loading = false;
          this.submitted = true;
          console.log('DONE !!!');
        }, error => {
          this.error = true;
          this.loading = false;
        });
    }, 4 * 1000)
  }

  onImgRemove = (i: number) => {
    this.screenshotsToUpload.splice(i, 1);
    this.screenshots.setValue(this.screenshotsToUpload);
    this.screenshots.updateValueAndValidity();
  }

  onTakeScreenshot = () => {
    html2canvas(document.body).then((canvas) => {
      this.screenshotsToUpload.push(canvas.toDataURL());
      this.screenshots.setValue(this.screenshotsToUpload);
      this.screenshots.updateValueAndValidity();
    });
  }

  onReset = () => {
    this.screenshotsToUpload = [];

    this.summary.reset('');
    this.description.reset('');
    this.screenshots.reset(this.screenshotsToUpload);
    this.log = "";
    this.priority.reset('low');
    this.issueType.reset('bug');
  }

  showValue() {
    let data = {
      summary: this.summary.value,
      description: this.description.value,
      screenshots: this.screenshotsToUpload.join('%'),
      logfile: "a file",
      date: this.date.value,
      priority: this.priority.value,
      issueType: this.issueType.value
    }
    console.log(data);
  }

  getDate = () => {
    let d = new Date(),
      hours = d.getHours().toString().length == 1 ? '0' + d.getHours() : d.getHours(),
      minutes = d.getMinutes().toString().length == 1 ? '0' + d.getMinutes() : d.getMinutes(),
      seconds = d.getSeconds().toString().length == 1 ? '0' + d.getSeconds() : d.getSeconds(),
      month = d.getMonth().toString().length == 1 ? '0' + d.getMonth() : d.getMonth();
    let fullDate = `${d.getFullYear()}-${month}-${d.getDate()} ${hours}:${minutes}:${seconds}`;
    return fullDate;
  }

  logout = () => {
    this.auth.logout();
  }

}
