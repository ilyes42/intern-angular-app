This repository represents the Front-end of my Issue Reporting web application project.
You can find the Back-end at: https://gitlab.com/ilyes42/sailsAPI

How to run this app?

- 1 - Clone the repository on your machine: git clone https://gitlab.com/ilyes42/intern-angular-app
- 2 - Change your directory: cd "intern-angular-app"
- 3 - Install all the dependencies: npm install
- 4 - Serve the app: ng serve --open
- 5 - You can find it on http://localhost:4000

How to build this app for production?

Repeat steps - 1 -, - 2 - and - 3 -, then:
- 4 - Build the app: ng build --prod
You can now find index.html with some javascript files under dist/intern-angular-app
If you open index.html and face a blank page then:
- open index.html
- change: base href="/", to: base href="./"